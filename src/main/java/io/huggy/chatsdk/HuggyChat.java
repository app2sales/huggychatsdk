package io.huggy.chatsdk;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class HuggyChat {
    private WebView webView;
    private String fcmToken;

    /*
    * Configure webview to enable javascript and
    * disable zoom support
    *
    * */
    public HuggyChat(Context context) {
        webView = new WebView(context);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
    }

    /*
     *   Create a widget without data
     *
     *   @param widget is a url to user widget
     *   @param fcmToken is a fcm token
     *
     *   @return void
     *
     * */
    public void createWidgetAnonymous(String widget, String fcmToken) {
        String url =
            widget +
            "&tokenFcm=" + fcmToken;
        this.fcmToken = fcmToken;
        webView.loadUrl(url);
    }

    /*
     *   Create a widget with previous data
     *
     *   @param widget is a url to user widget
     *   @param fcmToken is a fcm token
     *   @param name is a user name
     *   @param email is a user email
     *
     *   @return void
     *
     * */
    public void createWidgetWithData(String widget, String fcmToken, String name, String email) {
        String url =
            widget +
            "&tokenFcm=" + fcmToken +
            "&name=" + name +
            "&email=" + email;
        this.fcmToken = fcmToken;
        webView.loadUrl(url);
    }

    /*
     *   This function send a new token to Huggy
     *   for refresh token
     *
     *   @param oldToken ia a old token fcm
     *   @param newToken is a new token fcm
     *
     *   @return void
     *
     * */
    public WebView getWidget() {
        return webView;
    }

    /*
    *   This function send a new token to Huggy
    *   for refresh token
    *
    *   @param oldToken ia a old token fcm
    *   @param newToken is a new token fcm
    *
    *   @return void
    *
    * */
    public void changeToken(String newToken) {
        // Call API to change token
    }

}